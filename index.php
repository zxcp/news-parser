<?php

$time = microtime(true);

require_once('functions.php');
require_once('classes/phpQuery.php');
require_once('classes/News.php');
require_once('classes/DBDriver.php');

$months = getConfig('months');
$currentMonth = date('n');

$domain = 'https://www.td-kama.com';
$url = $domain . '/ru/main/news/';
$content = file_get_contents($url);
$content = phpQuery::newDocument($content);

$arrNews = new News();
foreach ($content->find('.news-reader .news-item') as $news) {
    $pq = pq($news);
    $href = $pq->find('a')->attr('href');
    $date = $pq->find('p.text-grey')->text();
    //'1 Мая 2018' => '1.05.2018' => timestamp
    $timestamp = strtotime(str_replace(' ' . $months[$currentMonth] . ' ', '.' . $currentMonth . '.', $date));

    if (!strpos($date, $months[$currentMonth])) {//Новость не за этот месяц
        if (!$arrNews->count()) {//Если за этот месяц нет новостей, то берём предыдущий
            $currentMonth--;
            continue;
        }
        break;
    }
    $arrNews->add(
        preg_replace('/[^0-9]/', '', $href),
        $pq->find('.news-item_text a')->text(),
        $domain . $href,
        getPreview($domain . $href),
        $domain . $pq->find('img')->attr('data-src'),
        $timestamp
    );
}
$arrNews->save();

echo 'Время выполнения скрипта: ' . number_format (microtime(true) - $time, 3) . 'сек.';

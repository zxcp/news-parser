<?php /** Created by Anton on 14.11.2018. */

/**
 * @param string $fileName имя файла конфигурации
 * @return array массив настроек
 */
function getConfig($fileName)
{
    $fileName = 'config/' . $fileName . '.php';
    if (!file_exists($fileName)) die('Не найден файл конфигурации: ' . $fileName);

    return include($fileName);
}

/**
 * @param string $href ссылка на новость
 * @return string первый абзац новости
 */
function getPreview($href)
{
    $detailPage = phpQuery::newDocument(file_get_contents($href));
    $p = pq($detailPage
        ->find('article p')
        ->get(0)
    )->text();
    return trim($p);
}

/**
 * Вывод массива данных
 * @param array $data
 */
function dd($data)
{
    echo '<pre>' . print_r($data, true) . '</pre><hr>';
//    exit;
}
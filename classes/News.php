<?php /** Created by Anton on 14.11.2018. */

class News
{
    protected $_news = [];

    /**
     * @param int $id id новости
     * @param string $title заголовок
     * @param string $url ссылка на новость
     * @param string $preview первый абзац новости
     * @param string $img ссылка на сизображение
     * @param int $date timestamp опубликования новости
     */
    public function add($id, $title, $url, $preview, $img, $date)
    {
        $this->_news[$id] = [$title, $url, $preview, $img, $date];
    }

    /** Сохраняет все спарсеные новости, кроме тех что уже содержатся в БД. */
    public function save()
    {
        $pdo = DBDriver::get();
        $stmt = $pdo->prepare('INSERT IGNORE INTO news (id, title, url, preview, img, date) VALUE (?, ?, ?, ?, ?, ?)');
        foreach ($this->_news as $id => $news) {
            array_unshift($news, $id);
            $stmt->execute($news);
        }
    }

    public function count()
    {
        return count($this->_news);
    }

}
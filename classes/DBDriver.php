<?php /** Created by Anton on 14.11.2018. */

class DBDriver
{
    protected static $_pdo = null;

    /** @return null|PDO */
    public static function get()
    {
        if (self::$_pdo === null) {
            $cfg = getConfig('db');
            $dsn = 'mysql:host=' . $cfg['host'] . ';dbname=' . $cfg['database'] . ';charset=' . $cfg['charset'];
            $opt = [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES   => false,
            ];
            self::$_pdo = new PDO($dsn, $cfg['username'], $cfg['password'], $opt);
        }
        return self::$_pdo;
    }
}

